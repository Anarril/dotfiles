#!/bin/bash

function toggle {
	WRKSPC_NR=$1
	i3-msg "workspace $WRKSPC_NR; layout toggle splith tabbed" > /dev/null
}

ORIG_WK_NAME=$(i3-msg -t get_workspaces \
  | jq '.[] | select(.focused==true).name' \
  | cut -d"\"" -f2)


toggle 0 \
  && toggle 1 \
  && toggle 2 \
  && toggle 3 \
  && toggle 4 \
  && toggle 5 \
  && toggle 6 \
  && toggle 7

i3-msg "workspace $ORIG_WK_NAME" > /dev/null


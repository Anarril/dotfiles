#!/bin/bash

### Prep/setup ###
ANTLION_NAME="alsa_input.usb-Antlion_Audio_Antlion_Wireless_Microphone-00.mono-fallback"
SAMSON_NAME="alsa_input.usb-Samson_Technologies_Samson_UB1-00.analog-stereo"
pactl upload-sample on.ogg
pactl upload-sample off.ogg

### Determine current status ###
ANTLION_NUMBER=$(pactl list short sources | grep $ANTLION_NAME | cut -f 1)
SAMSON_NUMBER=$(pactl list short sources | grep $SAMSON_NAME | cut -f 1)
ANTLION_MUTED=$(pactl list sources | grep "Antlion" -A 5 | grep "Mute" | cut -f 2)
SAMSON_MUTED=$(pactl list sources | grep $SAMSON_NAME -A 6 | grep "Mute" | cut -f 2)

echo $(pactl list sources | grep $SAMSON_NAME -A 6 | grep "Mute" | cut -f 2)

### Set desired status ###

if [ "$ANTLION_MUTED" = 'Mute: yes' ]
then
	echo "0"
        echo "🔊"
	pactl set-default-source $ANTLION_NAME;
	pactl play-sample on;
	pactl set-source-mute $ANTLION_NUMBER 0;
elif [ "$SAMSON_MUTED" = 'Mute: yes' ]
then	
	echo "1"
        echo "🔊"
	pactl set-default-source $SAMSON_NAME;
        pactl play-sample on;
        pactl set-source-mute $SAMSON_NUMBER 0;
else
	if [ $ANTLION_NUMBER ]
	then
        	echo "🔇"
		pactl play-sample off;
		pactl set-source-mute $ANTLION_NUMBER 1;
	elif [ $SAMSON_NUMBER ]
        then
        	echo "🔇"
		pactl play-sample off;
		pactl set-source-mute $SAMSON_NUMBER 1;
	fi	
fi

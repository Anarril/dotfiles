#!/bin/bash


DEFAULT_STATUS=$(pacmd list-sources | grep -e "\* index:" -A 20 | grep "muted")

if [ "$DEFAULT_STATUS" = '	muted: yes' ]
then
        echo "🔇"
else
        echo "🔊"
fi

exit 0;



## backup of old setup:


### Prep/setup ###
ANTLION_NAME="alsa_input.usb-Antlion_Audio_Antlion_Wireless_Microphone-00.mono-fallback"
pactl upload-sample on.ogg
pactl upload-sample off.ogg

### Determine current status ###
ANTLION_NUMBER=$(pactl list short sources | grep $ANTLION_NAME | cut -f 1)
ANTLION_MUTED=$(pactl list sources | grep "Antlion" -A 5 | grep "Mute" | cut -f 2)

### Set desired status ###
pactl set-default-source $ANTLION_NAME

if [ "$ANTLION_MUTED" = 'Mute: yes' ]
then
	echo "🔇"
else
	echo "🔊"
fi

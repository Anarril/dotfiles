#!/bin/bash

#!/bin/sh

WKNAME=$1

ORIG_WK_NAME=$(i3-msg -t get_workspaces \
  | jq '.[] | select(.focused==true).name' \
  | cut -d"\"" -f2)

if ! i3-msg -t get_workspaces | jq ".[] | .name" | grep -q $WKNAME ; then
  i3-msg "workspace $WKNAME; append_layout ~/.config/regolith/i3/empty_workspace.json" > /dev/null
  i3-msg "workspace $ORIG_WK_NAME" > /dev/null
fi



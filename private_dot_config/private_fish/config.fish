set -g fish_user_paths "/home/linuxbrew/.linuxbrew/bin" $fish_user_paths
set -g fish_user_paths "/home/johan/.local/bin" $fish_user_paths
set -g fish_user_paths "/home/johan/.composer/vendor/bin" $fish_user_paths

set -gx PATH $PATH $HOME/.krew/bin

set -gx CLOUDSDK_PYTHON (which python3)
set -gx COMPOSER_HOME /home/johan/.composer

set -gx EDITOR vi
alias nano=vi

abbr --add gco git checkout
abbr --add gcm git checkout master
abbr --add gp git pull
abbr --add gs git status
abbr --add g3 git log -n 3


abbr --add kl kubectl
abbr --add kx kubectx
abbr --add kn kubens

abbr --add f fuck

abbr --add ls lsd
abbr --add ll lsd -l
abbr --add lla lsd -la

abbr --add lad lazydocker
abbr --add lag lazygit
abbr --add 9 k9s

abbr --add cc chezmoi

alias cat=bat
alias watch=viddy


direnv hook fish | source

function hg
	history --reverse | grep "$argv"
end

function cx
	chmod +x $argv
end

function psa
	ps aux | grep "$argv"
end

function do
	/home/johan/projects/taskfile/taskfile/Taskmaster "$argv"
end

function ...
  set CURDIR (pwd);
  while test -d "$CURDIR" -a "$CURDIR" != '/'
    if test -x "$CURDIR/.git"
      cd "$CURDIR"
      break
    else
      set CURDIR (dirname "$CURDIR")
    end
  end
end
	


# Vagrant related

function vhaltall
	vboxmanage list runningvms | sed -r 's/.*\{(.*)\}/\1/' | xargs -L1 -I {} VBoxManage controlvm {} savestate
end


# Docker aliasses

function dockerps
    docker ps --format "table {{.Names}}\t{{.Image}}\t{{.CreatedAt}}\t{{.Status}}"
end

function dsls
   watch -n 1 docker service ls
end

function dsps
    watch -n 1 docker stack ps $argv
end

function drs
    docker run --volume (pwd):/volume -it $argv sh
end

# abbr --add dc docker compose
# abbr --add docker-compose "docker compose"

# function docker-compose
#    docker compose $argv
# end


# Docker related
function dive
   env CI=true docker run --rm -it \
      -v /usr/local/bin/docker:/bin/docker \
      -v /var/run/docker.sock:/var/run/docker.sock \
      -v  (pwd):(pwd) \
      -w (pwd) \
      -v "$HOME/.dive.yaml":"$HOME/.dive.yaml" \
      wagoodman/dive:latest $argv
end

function dv
   docker run --rm -it --name dcv -v (pwd):/input pmsipilot/docker-compose-viz $argv
end

function dvi
   dv render -m image --force docker-compose.yml
end



# Misc development tools

function km
  kubectl get nodes --no-headers | awk '{print $1}' | xargs -I {} sh -c 'echo {}; kubectl describe node {} | grep Allocated -A 5 | grep -ve Event -ve Allocated -ve percent -ve -- ; echo'
end


# function nvm
#  bass source ~/.nvm/nvm.sh --no-use ';' nvm $argv
# end

# nvm use --silent node
set -g fish_user_paths "/home/linuxbrew/.linuxbrew/sbin" $fish_user_paths

function pstorm
  phpstorm nosplash $argv &>/dev/null &; disown
end

source ~/.asdf/asdf.fish


#!/bin/bash

VPN_CONNECTED=$(nmcli c | grep enrise | awk '{ print $4 }')

if [ $VPN_CONNECTED == '--' ]
then
	echo "🔓"
else
	echo "🔐"
fi

